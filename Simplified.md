```mermaid
graph LR
    A[Physical Cluster] --> B(Virtual Cluster 1)
        A --> C(Virtual Cluster 2)

            B --> F[Tenant 1 App A]
                B --> G[Tenant 1 App B]
                    C --> H[Tenant 2 App C]
                        C --> I[Tenant 2 App D]

                            style A fill:#f9f,stroke:#333
                                style B,C fill:#ccf,stroke:#333
                                    style F,G,H,I fill:#afa,stroke:#333
                                    