```mermaid
graph LR
    A[Physical Kubernetes Cluster] --> B(Virtual Cluster 1)
        A --> C(Virtual Cluster 2)
            A --> D(Namespace 1)
                A --> E(Namespace 2)

                    B --> F[Tenant 1 - Application A]
                        B --> G[Tenant 1 - Application B]
                            C --> H[Tenant 2 - Application C]
                                C --> I[Tenant 2 - Application D]
                                    D --> J[Tenant 3 - Limited Access]
                                        E --> K[Tenant 4 - Limited Access]

                                            subgraph "Virtual Cluster 1"
                                                    F
                                                            G
                                                                end

                                                                    subgraph "Virtual Cluster 2"
                                                                            H
                                                                                    I
                                                                                        end

                                                                                            subgraph "Namespace 1"
                                                                                                    J
                                                                                                        end

                                                                                                            subgraph "Namespace 2"
                                                                                                                    K
                                                                                                                        end

                                                                                                                            style A fill:#f9f,stroke:#333,stroke-width:2px
                                                                                                                                style B,C fill:#ccf,stroke:#333
                                                                                                                                    style D,E fill:#eee,stroke:#333
                                                                                                                                        style F,G,H,I,J,K fill:#afa,stroke:#333

                                                                                                                                            L[Policy Engine] --> A
                                                                                                                                                M[Service Mesh] --> B
                                                                                                                                                    M --> C
                                                                                                                                                        N[Hierarchical Namespace Controller (HNC)] --> A
                                                                                                                                                            O[Platform Engineer] --> A

                                                                                                                                                                style L,M,N,O fill:#ffc,stroke:#333
                                                                                                                                                                